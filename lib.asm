%define stdin 0
%define stdout 1
%define stderr 2

global exit
global string_length
global print_string
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy


; Принимает код возврата и завершает текущий процесс
; rdi - аргумент (код возврата)
exit: 
    mov rax, 60
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
; rdi - аргумент (указатель на строку)
string_length:
    xor rax, rax
    
    .loop:
        cmp byte [rax + rdi], 0 ; конец строки
        je .end
        inc rax
        jmp .loop

    .end:
        ret ; rax содержит длину строки
    
; Принимает указатель на нуль-терминированную строку, выводит её в указанный поток
; rdi - аргумент (указатель на строку), rsi - поток
print_string:
    push rsi
    call string_length ; rax - длина строки
    mov rdx, rax
    mov rax, 1
    mov rsi, rdi
    pop rdi
    syscall
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 0xA

; Принимает код символа и выводит его в stdout
; rdi - аргумент (код символа)
print_char:
    push rdi
    mov rax, 1
    mov rsi, rsp
    mov rdi, stdout
    mov rdx, 1
    syscall
    pop rdi
    ret



; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
; rdi - аргумент (число)
print_uint:
    .save:
        push r12
        push r13

    mov rax, rdi
    mov r12, 10
    mov r13, rsp

    dec rsp
    mov [rsp], byte 0; записываем в стек нуль-терминатор

    .loop_div:
        xor rdx, rdx ; место для остатка
        div r12 ; делим rax на 10, остаток в rdx
        add dl, '0' ; переводим остаток в ASCII
        dec rsp
        mov [rsp], dl ; записываем в стек
        cmp rax, 0 ; сравнение с нулем
        jne .loop_div ; если rax = 0, то можно печатать

    .print_div:
        mov rdi, rsp
        call print_string
   
    .stop_restore:
        mov rsp, r13
        pop r13
        pop r12
        ret
    
; Выводит знаковое 8-байтовое число в десятичном формате 
; rdi - аргумент (число)
print_int:
    .save:
        push r12

    mov r12, rdi
    cmp rdi, 0
    jge .print_pos

    .print_neg: ; отрицательное число
        mov rdi, '-'
        call print_char ; выводим минус
        neg r12

    .print_pos: ; положительное число
        mov rdi, r12
        call print_uint
    
    .stop_restore:
        pop r12
        ret
  
; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
; rdi - аргумент (указатель на 1 строку)
; rsi - аргумент (указатель на 2 строку)
string_equals:
    .save:
        push r12
        push r13

    xor rcx, rcx ; смещение

    .loop:
        mov r12b, byte [rdi + rcx] ; символ 1 строки
        mov r13b, byte [rsi + rcx] ; символ 2 строки
        cmp r12b, r13b
        jne .not_equals
        cmp r12, 0 ; конец строки
        je .equals
        inc rcx
        jmp .loop

    .equals:
        mov rax, 1 ; 1 - они равны
        jmp .stop_restore    

    .not_equals:
        xor rax, rax ; 0 - они не равны
        jmp .stop_restore 

    .stop_restore:
        pop r13
        pop r12
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    push 0 
    xor rax, rax
    mov rdi, stdin
    mov rsi, rsp
    mov rdx, 1
    syscall

    pop rax ; rax содержит прочитанный символ
    ret
    
; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
; rdi - аргумент (адрес начала буфера)
; rsi - аргумент (размер буфера)

read_word:
    .save:
        push r12
        push r13
        push r14

    xor r12, r12 ; смещение
    mov r13, rsi ; размер буфера (read_char будет изменять rsi)
    mov r14, rdi ; адрес начала буфера (read_char будет изменять rdi)

    .loop_trash: ; пропуск пробельных символов вначале
        call read_char
        cmp rax, 0
        je .end_read
        cmp rax, 0x20
        je .loop_trash
        cmp rax, 0x9
        je .loop_trash
        cmp rax, 0xA
        je .loop_trash

    .loop_read:
        cmp r12, r13 ; больше не можем записывать (помним, что нам нужно еще дописать нуль-терминатор)
        je .error
        mov [r14 + r12], rax ; записываем в буфер символ
        inc r12
        call read_char
        cmp rax, 0
        je .end_read
        cmp rax, 0x20
        je .end_read
        cmp rax, 0x9
        je .end_read
        cmp rax, 0xA
        je .end_read
        jmp .loop_read

    .error:
        xor rax, rax ; 0 - неудача
        jmp .stop_restore

    .end_read:
        xor rax, rax
        mov [r14 + r12], rax ;  добавление нуль терминатора
        mov rax, r14 ; адрес буфера
        mov rdx, r12 ; длина слова

    .stop_restore:
        pop r14
        pop r13
        pop r12
        ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
; rdi - аргумент (указатель на строку)
parse_uint:
    .save:
        push r12
        push r13

    xor rax, rax ; будет хранить число
    xor rdx, rdx ; длина в символах
    mov r12, 10
    xor rcx, rcx ; смещение

    .loop_parse:
        mov r13b, byte [rdi + rcx] ; рассматриваемый символ
        cmp r13b, '0'
        jb .stop_restore
        cmp r13b, '9'
        ja .stop_restore
        sub r13b,'0' ; теперь в r13 находится цифра
        mul r12
        add rax, r13
        inc rcx
        jmp .loop_parse

    .stop_restore:
        mov rdx, rcx
        pop r13
        pop r12
        ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
; rdi - аргумент (указатель на строку)
parse_int:
    .save:
        push r12

    xor rax, rax ; число
    xor rdx, rdx ; длина
    mov r12b, byte [rdi] ; 1-ый символ
    cmp r12b, '-'
    je .parse_neg

    .parse_pos: ; положительное число
        call parse_uint ; rax - число, rdx - длина
        jmp .stop_restore

    .parse_neg: ; отрицательное число
        inc rdi ; учитываем -
        call parse_uint ; rax - число, rdx - длина
        cmp rdx, 0 ; если число не прочиталось
        je .stop_restore ; в rdx - 0, если переходим
        inc rdx ; учитываем знак

        neg rax ; теперь у нас знаковое число
        jmp .stop_restore

    .stop_restore:
        pop r12
        ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
; rdi - указатель на строку
; rsi - указатель на буфер
; rdx - длина буфера
string_copy:
    .save:
        push r12
        push r13

    call string_length ; rax - длина строки
    inc rax ; учитываем нуль-терминатор
    cmp rax, rdx 
    ja .fail ; не поместится в буфер
    xor r12, r12 ; смещение
    xor r13, r13 ; для символа строки

    .loop_copy:
        cmp r12, rax
        je .end_copy ; вся строка скопирована
        mov r13b, [rdi + r12] ; символ строки
        mov byte [rsi + r12], r13b ; записываем символ в буфер
        inc r12
        jmp .loop_copy

    .fail:
        xor rax, rax
        jmp .stop_restore

    .end_copy:
        dec r12 ; убираем нуль-терминатор
        mov rax, r12 ; длина строки
        jmp .stop_restore

    .stop_restore:
        pop r13
        pop r12
        ret
