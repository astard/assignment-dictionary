global find_word
extern string_equals
extern string_length

; Ищет ключ в словаре по его значению
; rdi - указатель на нуль-терминированную строку
; rsi - указатель на начало словаря (метка)
; rax - адрес начала вхождения в словарь, 0 - вхождения не найдено
find_word:
    .loop_search:
        add rsi, 8 ; в rsi - указатель на key
        call string_equals
        cmp rax, 1
        je .found_key 

        .not_found_key: ; ключи не равны
            sub rsi, 8 ; указатель на next_item
            mov rsi, [rsi] ; получили метку next_item
            cmp rsi, 0
            je .return
            jmp .loop_search
            .return:
                xor rax, rax ; 0 - вхождение не найдено
                ret

    .found_key: ; ключи равны
        call string_length ; rax - длина ключа
        inc rax ; учитываем нуль терминатор в памяти
        add rsi, rax ; указатель на значение
        mov rax, rsi ; указатель на value
        ret