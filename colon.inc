%define next_pointer 0

; 1 аргумент - ключ, 2 аргумент - метка
%macro colon 2
    %ifid %2:
        %ifstr %1:
            %%some_label: 
                dq next_pointer ; так как 8 байт на регистр
                %define next_pointer %%some_label
                db %1, 0
                %2: 
                    
        %else
            %error Неправильный ключ = %1
        %endif
    %else
        %error Неправильная метка = %2
    %endif
%endmacro
