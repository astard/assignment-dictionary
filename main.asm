%include "words.inc"
%define BUFFER_SIZE 256
%define stdout 1
%define stderr 2

global _start

section .rodata:
error_not_found_msg: db 'Значение по данному ключу не найдено', 0
error_buffer_overflow_msg: db 'Введенная строка больше чем 255 символов', 0

section .text
extern find_word
extern read_word
extern print_string
extern print_newline
extern exit

_start:
    sub rsp, BUFFER_SIZE ; выделили память для 256 символов (так как еще есть нуль терминатор)
    mov rdi, rsp ; адрес буфера
    mov rsi, BUFFER_SIZE ; размер буфера
    call read_word ; в rax - адрес строки, в rdx - длина
    cmp rax, 0
    je .error_buffer_overflow
    mov rdi, rax ; адрес строки
    mov rsi, next_pointer ; начало словаря
    call find_word ; rax - адрес вхождения или 0
    cmp rax, 0
    je .error_not_found

    .no_error: ; нашли вхождение
        mov rdi, rax
        mov rsi, stdout
        call print_string
        call print_newline
        mov rdi, 0; код возврата
        call exit

    .error_buffer_overflow: ; переполнение буфера
        mov rdi, error_buffer_overflow_msg
        mov rsi, stderr
        jmp .end_error

    .error_not_found: ; не нашли ключ
        mov rdi, error_not_found_msg
        mov rsi, stderr

    .end_error:
        call print_string
        call print_newline
        mov rdi, 1; код возврата
        call exit

